const Discord = require('discord.js');
const fetch = require('node-fetch');

require('dotenv').load();
process.env.APP_ENVIRONMENT = process.env.APP_ENVIRONMENT || 'local';

const config = require('./config.json');
config.color_api_key = process.env.COLOR_API_KEY;

const functions = require('./app/functions.js')(process.env);
const commands = require('./app/commands.js')(Discord, fetch, config, functions, process.env);

(function setupClient() {
    const client = new Discord.Client();
    let guild = null;
    let logChannel = null;

    client.on('ready', () => {
        functions.log('Ready!', 'success');

        guild = client.guilds.find(g => g.id == process.env.DISCORD_GUILD_ID);

        if (guild) {
            logChannel = guild.channels.find(c => c.id == process.env.DISCORD_LOG_CHANNEL_ID);

            if (logChannel) {
                client.on('messageUpdate', (originalMessage, updatedMessage) => {
                    if (guild == updatedMessage.guild && originalMessage.cleanContent !== updatedMessage.cleanContent && !updatedMessage.member.user.bot) {
                        const embed = new Discord.RichEmbed()
                            .setTimestamp((new Date))
                            .setAuthor(updatedMessage.author.username, updatedMessage.author.avatarURL, 'https://discordapp.com/users/' + updatedMessage.author.id)
                            .setColor('ff9900')
                            .setFooter('✏️ #' + updatedMessage.channel.name);

                        if (originalMessage.cleanContent.length > 1024) {
                            embed.addField('From (1/2)', originalMessage.cleanContent.substr(0, 1024));
                            embed.addField('From (2/2)', originalMessage.cleanContent.substr(1024, originalMessage.cleanContent.length));
                        } else {
                            embed.addField('From', originalMessage.cleanContent);
                        }

                        if (updatedMessage.cleanContent.length > 1024) {
                            embed.addField('To (1/2)', updatedMessage.cleanContent.substr(0, 1024));
                            embed.addField('To (2/2)', updatedMessage.cleanContent.substr(1024, updatedMessage.cleanContent.length));
                        } else {
                            embed.addField('To', updatedMessage.cleanContent);
                        }

                        logChannel.send(embed);
                    }
                });

                client.on('messageDelete', (message) => {
                    if (guild == message.guild && message.channel !== logChannel && !message.member.user.bot) {
                        if ((message.attachments.array().length) > 0) {
                            let embedCount = 0;
                            message.attachments.array().forEach(function (attachment) {
                                const embed = new Discord.RichEmbed()
                                    .setDescription((embedCount == 0 ? (message.cleanContent + ' ') : '') + attachment.proxyURL)
                                    .setTimestamp((new Date))
                                    .setAuthor(message.author.username, message.author.avatarURL, 'https://discordapp.com/users/' + message.author.id)
                                    .setColor('cc0000')
                                    .setFooter('🗑️ #' + message.channel.name)
                                    .setImage(attachment.proxyURL);

                                logChannel.send(embed);

                                embedCount++;
                            });
                        } else {
                            const embed = new Discord.RichEmbed()
                                .setDescription(message.cleanContent)
                                .setTimestamp((new Date))
                                .setAuthor(message.author.username, message.author.avatarURL, 'https://discordapp.com/users/' + message.author.id)
                                .setColor('cc0000')
                                .setFooter('🗑️ #' + message.channel.name);

                            if (message.type == 'GUILD_MEMBER_JOIN') {
                                embed.setAuthor(message.guild.name, message.guild.iconURL);
                                embed.setDescription('**' + message.author.username + '** has joined the server.');
                            }

                            logChannel.send(embed);
                        }
                    }
                });
            }
        }

        if (config.activity.length > 0) {
            client.user.setActivity(config.activity);
        }
    });

    client.on('message', message => {
        if (guild && guild == message.guild && message.content.startsWith(config.prefix)) {
            handleCommand(message);
        }

        if (message.author !== client.user && message.channel == logChannel) {
            message.delete();
        }
    });

    client.setTimeout(function () { // Restart client every 12 hours
        functions.log('Restarting...', 'warning');
        client.destroy().then(function() {
            setupClient();
        });
    }, ((12 * 60 * 60 * 1000) + 30));

    client.on("error", (e) => console.log(e));
    client.on("warn", (e) => console.log(e));

    client.login(process.env.DISCORD_TOKEN).then();
})();

function handleCommand(message) {
    const input = message.content.toLowerCase().substr(config.prefix.length);

    if (input.length > 0) {
        const parts = input.split(' ');

        const command = parts[0];
        let parameters = [];

        if (parts.length > 1) {
            parts.shift();
            parameters = parts;
        }

        if (typeof commands[command] !== 'undefined') {
            functions.debug('Running ' + command + ' command.', 'info', message.member);

            if ((commands[command].execute.length - 1) > parameters.length) {
                functions.debug('Command parameters missing.', 'warning', message.member);
                message.channel.send("Parameters missing.");
            } else {
                commands[command].execute(message, parameters);
            }
        }
    }
}