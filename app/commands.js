module.exports = function (Discord, fetch, config, functions, env) {
    const commands = {
        help: {
            execute: function (message) {
                const embed = new Discord.RichEmbed()
                    .setColor(config.color)
                    .setTitle("Available commands");

                Object.keys(commands).forEach(function (key) {
                    //console.log(message);
                    if (commands[key].condition(message)) {
                        embed.addField(config.prefix + commands[key].command, commands[key].description);
                    }
                });

                message.channel.send(embed);
            },
            command: 'help',
            description: 'Display help dialogue.',
            condition: (m) => true
        },
        list: {
            execute: function (message) {
                const guildRoles = functions.getAssignableRoles(message.guild.roles);

                const files = [];
                const order = [2, 3, 0, 1, 6, 7, 4, 5, 8, 9]; //Attach files in this order to display correctly
                let o = 0;

                guildRoles.forEach(function (role) {
                    const roleColor = role.color == 0 ? 'ffffff' : role.color.toString(16);
                    const attachmentUrl = config.color_api + '/' + roleColor + '/' + role.name + '/' + config.color_api_key;

                    files.push({
                        attachment: attachmentUrl,
                        name: order[o] + '.png'
                    });

                    o = (o == 9) ? 0 : (o + 1);
                });

                if (files.length > 0) {
                    const chunkSize = 10;
                    const fileChunks = [];

                    for (let i = 0; i < files.length; i += chunkSize) { //Split files into chunks to handle the file limit
                        fileChunks.push(files.slice(i, i + chunkSize));
                    }

                    functions.debug('Listing ' + files.length + ' available roles in ' +
                        fileChunks.length + ' batch' + (fileChunks.length > 1 ? 'es' : '') + '.', 'info', message.member);

                    let j = 0;
                    function sendBatchMessage() {
                        if (j < fileChunks.length) {
                            message.channel.send((j == 0 ? 'Available roles:' : ''), {files: fileChunks[j]}).then(function () {
                                functions.debug('Role batch ' + (j + 1) + ' sent.', 'success', message.member);
                                j++;
                                sendBatchMessage();
                            }, function (rejection) {
                                functions.debug(rejection, 'danger')
                            });
                        }
                    }
                    sendBatchMessage();
                } else {
                    functions.debug('No roles available.', 'warning', message.member);
                    message.channel.send('No roles available.');
                }
            },
            command: 'list',
            description: 'Display listing of available roles.',
            condition: (m) => true
        },
        set: {
            execute: function (message, parameters) {
                const roleInput = parameters.join(' ');
                const assignableRoles = functions.getAssignableRoles(message.guild.roles);
                const assignableRolesByName = functions.groupRolesByName(assignableRoles);

                if (typeof assignableRolesByName[roleInput] !== 'undefined') {
                    functions.replaceMemberRoles(message.member, functions.getIdListFromRoles(assignableRoles), assignableRolesByName[roleInput]);
                    message.channel.send('Role set.');
                } else {
                    message.channel.send('Role not found.');
                }
            },
            command: 'set <role>',
            description: 'Set your active color role to <role>.',
            condition: (m) => true
        },
        regular: {
            findRole: (m) => m.guild.roles.find((item) => item.id == env.REGULAR_ROLE),
            execute: function (message) {
                if (typeof env.REGULAR_ROLE !== 'undefined' && this.findRole(message)) {
                    console.log('role found');

                    if (message.member.roles.find(function(item) {
                            return item.id == env.REGULAR_ROLE;
                        })) {
                        message.member.removeRole(this.findRole(message)).then(() => {
                            message.channel.send('Regular role removed.');
                        })
                    } else {
                        functions.replaceMemberRoles(message.member, [], this.findRole(message));
                        message.channel.send('Regular role added.');
                    }

                }
            },
            command: 'regular',
            description: 'Assign regular role. Role will be removed from inactive users.',
            condition: (m) => (typeof env.REGULAR_ROLE !== 'undefined' && m.guild.roles.find((item) => item.id == env.REGULAR_ROLE))
        },
        randomuser: {
            execute: function(message) {
                const onlineMembers = [];

                message.guild.members.array().forEach(function (member) {
                    if (member.presence.status === 'online' && !member.user.bot && member !== message.member) {
                        onlineMembers.push(member);
                    }
                });

                if (onlineMembers.length > 0) {
                    const randomMember = onlineMembers[Math.floor(Math.random() * onlineMembers.length)];
                    const memberMessage = randomMember.displayName + ' `@' + randomMember.user.username + '#' + randomMember.user.discriminator + '` ' + randomMember.user.id;

                    functions.debug('Found ' + memberMessage, 'success', message.member);
                    message.channel.send(memberMessage);
                } else {
                    functions.debug('No member found', 'warning', message.member);
                    message.channel.send('No member found.');
                }

            },
            command: 'randomuser',
            description: 'Get a random online user.',
            condition: (m) => true
        },
        gdq: {
            execute: function (message) {
                const startTime = + new Date();
                fetch('http://taskinoz.com/gdq/api')
                    .then(res => res.text())
                    .then(body => {
                        const endTime = + new Date();
                        message.channel.send(body + ' (' + (endTime - startTime) + 'ms)');
                    });
            },
            command: 'gdq',
            description: 'Games Done Quick donation generator.',
            condition: (m) => true
        }
    };

    return commands;
};