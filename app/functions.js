module.exports = function (env) {
    const functions = {
        roleIsAssignable: function (role) {
            if (typeof env.ROLE_COMPARE !== 'undefined') {
                if (role.guild.roles.find(function(item) {
                        return item.id == env.ROLE_COMPARE;
                    })) {
                    if (role.comparePositionTo(role.guild.roles.find('id', env.ROLE_COMPARE)) >= 0) {
                        return false;
                    }
                }
            }

            if (typeof env.EXCLUDED_ROLES !== 'undefined' && functions.isJsonString(env.EXCLUDED_ROLES)) {
                const excludedRoles = JSON.parse(env.EXCLUDED_ROLES);

                if (excludedRoles.indexOf(parseInt(role.id)) > -1) {
                    return false;
                }
            }

            return (role.color !== 0 &&
            !role.hoist &&
            !role.hasPermission('ADMINISTRATOR') &&
            !role.hasPermission('KICK_MEMBERS') &&
            !role.hasPermission('BAN_MEMBERS') &&
            !role.hasPermission('MANAGE_CHANNELS') &&
            !role.hasPermission('MANAGE_GUILD') &&
            !role.hasPermission('MANAGE_MESSAGES') &&
            !role.hasPermission('MANAGE_NICKNAMES') &&
            !role.hasPermission('MANAGE_ROLES') &&
            !role.hasPermission('MANAGE_WEBHOOKS') &&
            !role.hasPermission('MANAGE_EMOJIS'));
        },
        getAssignableRoles: function (roles) {
            const sortedRoles = roles.sort(function(a, b){
                if(a.position > b.position) return -1;
                if(a.position < b.position) return 1;
                return 0;
            });

            const assignableRoles = [];

            sortedRoles.array().forEach(function (role) {
                if (functions.roleIsAssignable(role)) {
                    assignableRoles.push(role);
                }
            });

            return assignableRoles;
        },
        groupRolesByName: function (roles) {
            const groupedNames = [];

            roles.forEach(function (role) {
                groupedNames[role.name.toLowerCase()] = role;
            });

            return groupedNames;
        },
        getIdListFromRoles: function (roles) {
            return roles.map(function (role) {
                return role.id;
            })
        },
        replaceMemberRoles: function(member, rolesToRemove, roleToAdd) {
            functions.debug('Attempting to remove old roles from user.', 'info', member);

            member.removeRoles(rolesToRemove).then(function () {
                functions.debug('Old roles removed.', 'success', member);

                let addRoleAttempts = 1;

                (function addRoleInterval() {
                    if (!member.roles.has(roleToAdd.id) && addRoleAttempts <= 5) {
                        functions.debug('Attempting to add new role to user.', 'info', member);

                        member.addRole(roleToAdd);

                        addRoleAttempts++;

                        setTimeout(addRoleInterval, (5 * 1000));
                    } else {
                        functions.debug('New role added.', 'success', member);
                    }
                })();
            });
        },
        padString: function (string, padding = '00') {
            string += "";
            return padding.substr(0, padding.length - string.length) + string;
        },
        dateTime: function (dateObject = new Date()) {
            const year = dateObject.getFullYear();
            const month = dateObject.getMonth() + 1;
            const day = dateObject.getDate();

            const date = year + '_' + functions.padString(month) + '_' + functions.padString(day);

            const hours = dateObject.getHours();
            const minutes = dateObject.getMinutes();
            const seconds = dateObject.getSeconds();

            const time = functions.padString(hours) + ':' + functions.padString(minutes) + ':' + functions.padString(seconds);

            return date + ' ' + time;
        },
        isJsonString: function (string) {
            try {
                JSON.parse(string);
            } catch (e) {
                return false;
            }
            return true;
        },
        log: function (message, type) {
            if (process.env.APP_ENVIRONMENT == 'heroku') {
                console.log(message);
                return;
            }

            const colors = {
                red: "\x1b[31m%s\x1b[0m",
                green: "\x1b[32m%s\x1b[0m",
                yellow: "\x1b[33m%s\x1b[0m",
                blue: "\x1b[34m%s\x1b[0m",
                magenta: "\x1b[35m%s\x1b[0m",
                cyan: "\x1b[36m%s\x1b[0m"
            };

            message = '<' + functions.dateTime() + '> ' + message;

            switch (type) {
                case 'info':
                    console.log(colors.cyan, message);
                    break;
                case 'success':
                    console.log(colors.green, message);
                    break;
                case 'warning':
                    console.log(colors.yellow, message);
                    break;
                case 'danger':
                    console.log(colors.red, message);
                    break;
                case 'debug':
                    console.log(colors.magenta, message);
                    break;
                default:
                    console.log(message);
                    break;
            }
        },
        debug: function (message, type, sentByMember = false) {
            if (env.APP_DEBUG == 'true') {
                if (sentByMember) {
                    message = sentByMember.user.username + '#' + sentByMember.user.discriminator + ': ' + message;
                }

                functions.log(message, type);
            }
        }
    };

    return functions;
};